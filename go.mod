module evigo

go 1.14

require (
	github.com/Nerzal/gocloak/v5 v5.2.1
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/flosch/pongo2 v0.0.0-20200509134334-76fc00043fe1 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-bongo/go-dotaccess v0.0.0-20190924013105-74ea4f4ca4eb // indirect
	github.com/go-macaron/binding v1.1.0
	github.com/go-macaron/csrf v0.0.0-20200329073418-5d38f39de352
	github.com/go-macaron/inject v0.0.0-20200308113650-138e5925c53b // indirect
	github.com/go-macaron/pongo2 v0.0.0-20200329073512-6ca146b415a1
	github.com/go-macaron/session v0.0.0-20200329073812-7d919ce6a8d2
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/goonode/mogo v0.0.0-20181028112152-10c38e9be609
	github.com/gotk3/gotk3 v0.4.0
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f // indirect
	github.com/oleiade/reflections v1.0.0 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/net v0.0.0-20200520004742-59133d7f0dd7 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/appengine v1.6.6 // indirect
	gopkg.in/ini.v1 v1.56.0 // indirect
	gopkg.in/macaron.v1 v1.3.8
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
