package adminrpc

import (
	"context"
	"errors"
	"evigo/db"
	"evigo/web"
	"fmt"
	"github.com/coreos/go-oidc"
	"github.com/globalsign/mgo/bson"
	"gopkg.in/macaron.v1"
	"log"
	"net/http"
	"strings"
)

type AdherentAdminServiceRPC struct{}

var webrpcserver WebRPCServer
var verifier *oidc.IDTokenVerifier
var validatedTokens map[string]bool

func Init() WebRPCServer {
	webrpcserver = NewAdherentAdminServiceServer(&AdherentAdminServiceRPC{})
	verifier = oidc.NewVerifier(
		fmt.Sprintf("%s/auth/realms/asso-insa-lyon", web.KeycloakUrl),
		oidc.NewRemoteKeySet(context.TODO(), fmt.Sprintf("%s/auth/realms/asso-insa-lyon/protocol/openid-connect/certs", web.KeycloakUrl)),
		&oidc.Config{ClientID: "account"}, //correspond à l'audience attendue. account est l'audience reçue pour un appel client credentials
	)
	validatedTokens = map[string]bool{}
	return webrpcserver
}
func VerifyToken(token string) bool {
	autorise, ok := validatedTokens[token]
	if ok {
		return autorise //ça blackliste quelqu'un dont le token est invalide
	} else {
		idtoken, err := verifier.Verify(context.TODO(), token)
		if err != nil {
			log.Print(err)
			return false
		}
		var claims web.Claims
		if err := idtoken.Claims(&claims); err != nil {
			return false
		}
		validatedTokens[token] = claims.IsBureau()
		return claims.IsBureau()
	}
}
func ServeRPC(ctx *macaron.Context, w http.ResponseWriter, r *http.Request) {
	if ctx.Req.Method == "POST" && strings.HasPrefix(ctx.Req.URL.String(), "/rpc/") {
		rawidtoken := ctx.Req.Header.Get("Keycloak")

		if VerifyToken(rawidtoken) {
			webrpcserver.ServeHTTP(w, r)
		} else {
			ctx.Error(403, "Vous ne faites pas partie du Bureau")
		}
	}
}
func ToRPC(in *db.Adherent) *Adherent {
	return &Adherent{
		Username:  in.Username,
		Email:     in.Email,
		FirstName: in.FirstName,
		LastName:  in.LastName,
		Valide:    in.Valide,
	}
}
func (changed *Adherent) UpdateFromRPC(stored *db.Adherent) {
	stored.Username = changed.Username
	stored.Email = changed.Email
	stored.FirstName = changed.FirstName
	stored.LastName = changed.LastName
	stored.Valide = changed.Valide
}

func (s *AdherentAdminServiceRPC) Ping(ctx context.Context) error {
	return nil
}

func (s *AdherentAdminServiceRPC) ListValidAdherents(ctx context.Context) ([]*Adherent, error) {
	ins := db.ListValidAdherents()
	ret := make([]*Adherent, len(ins))
	for i, in := range ins {
		ret[i] = ToRPC(in)
	}

	return ret, nil
}
func (s *AdherentAdminServiceRPC) ListAdherents(ctx context.Context, filter *ListFilter) ([]*Adherent, error) {
	var res []*db.Adherent
	err := db.AdherentDocument().Find(bson.D{
		{"a_paye", filter.APaye},
		{"valide", filter.Valide},
	}).All(&res)

	ret := make([]*Adherent, len(res))
	for i := 0; i < len(res); i++ {
		ret[i] = ToRPC(res[i])
	}
	return ret, err
}
func (s *AdherentAdminServiceRPC) FindAdherents(ctx context.Context, search *Filter) ([]*Adherent, error) {
	return nil, errors.New("aghah")
}
func (s *AdherentAdminServiceRPC) UpdateAdherent(ctx context.Context, updated *Adherent) error {
	old, err := db.FindAdherentByUsername(updated.Username)
	if err != nil {
		return err
	}
	updated.UpdateFromRPC(old)
	return old.Save()
}
