package main

import (
	"evigo/adminrpc"
	"evigo/db"
	"evigo/web"
)

func main() {

	m := web.CreateServer() //  https://go-macaron.com/

	web.SetupRemoteAuth() //initialise la connexion à Keycloak

	db.Connect()
	defer db.Disconnect()

	web.SetupRoutes(m)

	adminrpc.Init()
	m.Use(adminrpc.ServeRPC)

	m.Run()
}
