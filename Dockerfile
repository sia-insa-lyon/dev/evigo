FROM golang:1.14 as builder

WORKDIR /go/src
RUN mkdir -p evigo

COPY go.mod evigo
COPY go.sum evigo

# téléchargement des dépendances
RUN cd evigo && go mod tidy

COPY db evigo/db
COPY main.go evigo
COPY web evigo/web
COPY adminrpc evigo/adminrpc
COPY adminclient evigo/adminclient
# une 2e fois au cas où des deps n'étaient pas ajoutées à dummypkg.go

# build Go
RUN CGO_ENABLED=0 GOOS=linux cd evigo && go build -a -installsuffix cgo -o main main.go

FROM alpine:3.11
COPY --from=builder /go/src/evigo/main .
COPY templates templates
COPY static static
RUN cd static && wget "https://code.jquery.com/jquery-3.5.1.slim.min.js" && wget "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" && wget "https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/flatly/bootstrap.min.css"

EXPOSE 4000
ENV MONGO_URL "mongodb://127.0.0.1:27017"
ENV DATABASE_NAME "evigo"
ENV MACARON_ENV "production"
CMD ["/main"]