# Explications
Ce serveur (écrit en Go) utilise une base de données MongoDB et se connecte à Keycloak.

Il récupère les profils adhérents depuis Keycloak quand ceux-ci se connectent en OpenIDconnect.
Si un utilisateur possède le rôle ``"bureau"``, il est administrateur.

Les profils utilisateurs sont mis à jour à chaque connexion. Il n'existe pas d'autre mode de connexion que Keycloak (ou autres Openidconnect).
## Permissions
Mode alternatif où les adhérents n'ont pas besoin de se connecter
On truste les admins d'asso et on leur permet de rechercher par email dans la base de données.
C'est forcé dans l'appli ...

## Fonctionnement
Utilise le *framework* **go-macaron**, qui utilise le même principe de Handlers et middlewares que `net/http`.
Le routage est interopérable avec des Handlers Go `net/http` classiques.

Les templates sont faits avec **pongo2**, qui a la même syntaxe que les templates *Django*

La base de données est **MongoDB**, du *NoSQL*.
# Déploiement
## Configuration
### Variables d'environnment
```shell script
KEYCLOAK_URL="https://sso.asso-insa-lyon.fr/auth/realms/asso-insa-lyon"
CLIENT_ID=""
CLIENT_SECRET="1337-secret-4242-hax0r"
BASE_URL=""
```
### Keycloak
Assigner à un groupe Bureau AMI le rôle client goami/bureau.
### Édition des permissions
Attention, les actions effectuées par le serveur Go (mode KC *Client*) demandent
des permissions non seulement dans l'onglet *Service Account Roles*, mais il faut mettre les mêmes dans *Scope*.
(on peut aussi demander *Full Scope allowed*, mais c'est pas ouf).

# Développement
## Dépendances
Ce projet utilise Go Modules (`go mod`)
Après avoir touché aux dépendances, ajouté des *imports* ..., lancer `go mod tidy`

## Web-rpc
Mettre à jour le fichier `adminrpc/interface.ridl`
```shell script
webrpc-gen -schema=interface.ridl -target=go -pkg adminrpc -server -client -out=./interface.gen.go
```

Pour installer webrpc: ``go get -u github.com/webrpc/webrpc/cmd/webrpc-gen``