package db

import (
	"fmt"
	"github.com/globalsign/mgo/bson"
)

func FindAdherentByUsername(username string) (*Adherent, error) {
	adherent := InitAdherent()
	err := AdherentDocument().FindOne(bson.D{
		{"username", username},
	}, adherent)

	if err != nil {
		return nil, err
	}
	return adherent, nil
}

func CreateAdherent(adherent *Adherent) {
	handleError(adherent.Save())
}

func UpdateAdherent(adherent *Adherent) *Adherent {
	handleError(adherent.Save())
	return adherent
}

// Renvoie tous les adhérents qui ont payé
func ListValidAdherents() []*Adherent {
	q := AdherentDocument().Find(
		bson.D{
			{"a_paye", true},
			{"valide", true},
		})
	var valides []*Adherent
	handleError(q.All(&valides))
	return valides
}

func ListUnpaidAdherents() []*Adherent {
	var non_payes []*Adherent
	q := InitAdherent().Find(bson.D{
		{"a_paye", false},
		{"valide", true},
	})
	handleError(q.All(&non_payes))
	return non_payes
}
func ListInvalidAdherents() []*Adherent {
	var invalides []*Adherent
	q := InitAdherent().Find(bson.D{
		{"valide", false},
	})
	handleError(q.All(&invalides))
	return invalides
}
func UpdateMultiplePayments(usernames []string) {
	if len(usernames) == 0 {
		fmt.Println("0 adhérent sélectionnés")
		return
	}
	filter := bson.D{{"username", bson.D{{"$in", usernames}}}}

	update := bson.D{{"$set", bson.D{
		{"a_paye", true},
		{"valide", true},
	}}}
	_, err := InitAdherent().GetColl().C().UpdateAll(filter, update)
	handleError(err)
}
func UpdateMultipleValidations(usernames []string) {
	if len(usernames) == 0 {
		fmt.Println("0 adhérent sélectionnés")
		return
	}
	filter := bson.D{{"username", bson.D{{"$in", usernames}}}}

	update := bson.D{{"$set", bson.D{
		{"valide", true},
	}}}
	_, err := InitAdherent().GetColl().C().UpdateAll(filter, update)
	handleError(err)
}
