package db

import (
	"encoding/gob"
	"github.com/goonode/mogo"
	"log"
	"os"
)

var Connection *mogo.Connection

func Connect() {
	uri := os.Getenv("MONGO_URL")
	if uri == "" {
		uri = "mongodb://localhost:27017"
	}
	databaseName := os.Getenv("DATABASE_NAME")
	if databaseName == "" {
		databaseName = "evigo"
	}
	log.Printf("Connexion à to %s", uri)
	config := &mogo.Config{
		ConnectionString: uri,
		Database:         databaseName,
	}
	var err error
	Connection, err = mogo.Connect(config)
	handleError(err)

	log.Print("connecté à Mongo")

	gob.Register(Adherent{})
	mogo.ModelRegistry.Register(Adherent{})
}
func Disconnect() {
	Connection.Session.Close()
}

func handleError(err error) {
	if err != nil {
		log.Panic(err)
	}
}
