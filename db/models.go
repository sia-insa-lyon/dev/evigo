package db

import (
	"github.com/Nerzal/gocloak/v5"
	"github.com/goonode/mogo"
	"time"
)

type IdToken struct {
	AuthToken    string
	RefreshToken string
}

//proteus:generate
type Adherent struct { //les champs CamelCase sont convertis en lowercase
	mogo.DocumentModel `bson:",inline" coll:"adherents"`
	//récup de keycloak
	Username  string  `idx:"{username},unique"`
	Email     string  ``
	Auth      IdToken ``
	FirstName string  ``
	LastName  string
	Gender    string `` //c'est un attribut sur KC

	JoinedAt     time.Time `bson:"joined_at"`
	Commentaires string    //pour les admins
	APaye        bool      `bson:"a_paye"`
	Valide       bool      `bson:"valide"`
	RoleBureau   bool      `bson:"role_bureau"`
}

func AdherentDocument() *Adherent {
	return mogo.NewDoc(Adherent{}).(*Adherent)
}
func InitAdherent() *Adherent {
	adherent := AdherentDocument()
	adherent.Commentaires = "ajouté par Keycloak"
	adherent.JoinedAt = time.Now()
	adherent.APaye = false
	adherent.Valide = false
	return adherent
}
func (this *Adherent) UserToAdherent(user *gocloak.User) {
	this.LastName = *user.LastName
	this.FirstName = *user.FirstName
	this.Email = *user.Email
	this.Username = *user.Username
	for key, values := range user.Attributes {
		if key == "gender" {
			this.Gender = values[0]
		}
	}
}
func (this *Adherent) DisplayGender() string {
	if this.Gender == "W" {
		return "femme"
	}
	if this.Gender == "M" {
		return "homme"
	} else {
		return "_"
	}
}

func (this *Adherent) String() string {
	return this.FirstName + " " + this.LastName
}
