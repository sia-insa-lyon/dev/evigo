package db

type AdherentForm struct {
	//Username     string `form:"username" binding:"Required"`
	Commentaires string `form:"commentaires"`
	Valide       bool   `form:"valide"`
	APaye        bool   `form:"a_paye"`
}

func (adherent *Adherent) ApplyFormData(form *AdherentForm) {
	adherent.Commentaires = form.Commentaires
	adherent.APaye = form.APaye
	adherent.Valide = form.Valide
}
