package main

import (
	"context"
	"encoding/base64"
	"evigo/adminrpc"
	"fmt"
	"github.com/Nerzal/gocloak/v5"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"log"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"sync"
	"time"
)

const (
	COLONNE_INV_USERNAME = iota
	COLONNE_INV_EMAIL    = iota
	COLONNE_INV_PRENOM   = iota
	COLONNE_INV_NOM      = iota
)

//export PATH="/snap/go/current/bin/:$PATH"
var client adminrpc.AdherentAdminService
var httpClient HTTPClient
var access_token string
var localAdherent map[string]*adminrpc.Adherent

type HTTPClient struct { //pour customiser le RealClient http
	RealClient *http.Client
}

func (h *HTTPClient) Do(req *http.Request) (*http.Response, error) {
	req.Header.Set("Keycloak", access_token)
	fmt.Printf("url client : %s, header KC: %s\n", req.URL, req.Header.Get("Keycloak"))
	return h.RealClient.Do(req)
}

func (h *HTTPClient) SetKeycloakToken(jwt *gocloak.JWT) {
	access_token = jwt.AccessToken
}
func NewClient(auth string) *HTTPClient {
	auth = "lkjhlkjhlkjhlkjhlkjhlkjh"
	return &HTTPClient{
		RealClient: &http.Client{},
	}
}
func setupClient() {
	url := os.Getenv("EVIGO_URL")
	if url == "" {
		url = "http://127.0.0.1:4000"
	}
	httpClient := NewClient("lkhlkhlkjhlkjh")
	client = adminrpc.NewAdherentAdminServiceClient(url, httpClient)
	//RealClient = adminrpc.NewAdherentAdminServiceClient("http://192.168.0.25:4000", &http.Client{})
	client.Ping(context.TODO())
	log.Print("rpc server responded to ping")
	ads, e := client.ListValidAdherents(context.TODO())
	if e != nil {
		log.Printf("%d adhérents\n", len(ads))
	}
	localAdherent = map[string]*adminrpc.Adherent{}
}

func setupMainAppWindow(builder *gtk.Builder) *gtk.ApplicationWindow {

	wino, err := builder.GetObject("app_window")
	handleError(err)
	win := wino.(*gtk.ApplicationWindow)

	at, err := builder.GetObject("main_notebook")
	handleError(err)
	main_notebook := at.(*gtk.Notebook)
	main_notebook.Show()

	rb, err := builder.GetObject("refresh_button")
	handleError(err)
	refresh_button := rb.(*gtk.Button)
	refresh_button.Show()

	liv, err := builder.GetObject("tree_adherents")
	handleError(err)
	tree_adherents := liv.(*gtk.TreeView)
	tree_adherents.Show()

	list_valid := fst(builder.GetObject("list_valid")).(*gtk.CheckButton)
	list_apaye := fst(builder.GetObject("list_apaye")).(*gtk.CheckButton)

	win.SetKeepAbove(true)

	cr, err := gtk.CellRendererTextNew()
	colonne_username, _ := gtk.TreeViewColumnNewWithAttribute("Username", cr, "text", COLONNE_INV_USERNAME)
	colonne_username.SetClickable(true)
	colonne_email, _ := gtk.TreeViewColumnNewWithAttribute("E-mail", cr, "text", COLONNE_INV_EMAIL)
	colonne_prenom, _ := gtk.TreeViewColumnNewWithAttribute("Prénom", cr, "text", COLONNE_INV_PRENOM)
	colonne_nom, _ := gtk.TreeViewColumnNewWithAttribute("Nom", cr, "text", COLONNE_INV_NOM)
	adherents, err := gtk.ListStoreNew(glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING)
	handleError(err)
	tree_adherents.SetModel(adherents)

	tree_adherents.AppendColumn(colonne_username)
	tree_adherents.AppendColumn(colonne_email)
	tree_adherents.AppendColumn(colonne_prenom)
	tree_adherents.AppendColumn(colonne_nom)

	updateAdherentsTree := func() {
		// met à jour la vue de la liste pour représenter les changements depuis la modale d'édition
		// là on reset tout, mais on pourrait mettre à jour uniquement la ligne précedemment sélectionnée
		glib.IdleAdd(func() {
			iter, _ := adherents.GetIterFirst()
			doNetxt := true
			for doNetxt {
				val, _ := adherents.GetValue(iter, COLONNE_INV_USERNAME)
				username, _ := val.GetString()
				adherent := localAdherent[username]
				handleError(adherents.Set(iter, []int{
					COLONNE_INV_USERNAME,
					COLONNE_INV_EMAIL,
					COLONNE_INV_PRENOM,
					COLONNE_INV_NOM,
				}, []interface{}{
					adherent.Username,
					adherent.Email,
					adherent.FirstName,
					adherent.LastName,
				}))
				doNetxt = adherents.IterNext(iter)
			}
		})
	}

	tree_adherents.Connect("row-activated", func(tv *gtk.TreeView, path *gtk.TreePath, column *gtk.TreeViewColumn) {
		iter, err := adherents.GetIter(path)
		handleError(err)
		uval, _ := adherents.GetValue(iter, COLONNE_INV_USERNAME)
		username, _ := uval.GetString()
		adherent, ok := localAdherent[username]
		if ok {
			ModalDisplayData(adherent, func() {
				win.Present()
				updateAdherentsTree()
			})
		}
	})
	updateLocalAdherents := func() {
		go func() {
			t, _ := tree_adherents.GetSelection() // pour
			_, i, _ := t.GetSelected()            //sauvegarder la sélection
			p, _ := adherents.GetPath(i)          // quand on refresh

			db_rows, err := client.ListAdherents(context.TODO(), &adminrpc.ListFilter{
				Valide: list_valid.GetActive(),
				APaye:  list_apaye.GetActive(),
			})
			displayError(&win.Window, err)
			for _, adherent := range db_rows {
				localAdherent[adherent.Username] = adherent
			}
			adherents.Clear()
			for _, adherent := range localAdherent {
				handleError(adherents.Set(adherents.Append(), []int{
					COLONNE_INV_USERNAME,
					COLONNE_INV_EMAIL,
					COLONNE_INV_PRENOM,
					COLONNE_INV_NOM,
				}, []interface{}{
					adherent.Username,
					adherent.Email,
					adherent.FirstName,
					adherent.LastName,
				}))
				println(adherent.Username)
			}
			tree_adherents.ShowAll()
			tree_adherents.Show()

			t.SelectPath(p) //restaure la sélection
		}()
	}

	/*handleTabChange := func() {
		glib.IdleAdd(func() {
			log.Println("handling tab change maybe")
			println(main_notebook.GetCurrentPage())
			if main_notebook.GetCurrentPage() == 0 { //tab valides
				updateAdherentsTree()
			}
			if main_notebook.GetCurrentPage() == 1 {
			}
			if main_notebook.GetCurrentPage() == 2 {
			}
		})
	}*/

	//main_notebook.Connect("switch-page",handleTabChange)
	refresh_button.Connect("clicked", updateLocalAdherents)

	go func() {
		time.Sleep(time.Millisecond * 200)
		updateLocalAdherents()
	}()
	win.ShowAll()
	return win
}
func PreflightCheck() {
	_, err := os.Open("gschemas.compiled")
	if err != nil {
		println("not heere")
		file, err := os.Create("gschemas.compiled")
		handleError(err)
		source := []byte("R1ZhcmlhbnQAAAAAAAAAABgAAABYAAAAAAAAKAIAAAAAAAAAAQAAAKIAnMsBAAAAWAAAAC4ASACIAAAAAAEAAAUVAAD/////aAEAAAAATABoAQAAbAEAAGNvbS5naXRsYWIuc2lhLWluc2EtbHlvbi5kZXYuZXZpZ28uYWRtaW5jbGllbnQAAAAAACgEAAAAAAAAAAIAAAAEAAAABAAAAMB9fgv/////AAEAAAUAdgAIAQAAOwEAADjc9hcDAAAAOwEAAAgAdgBIAQAATQEAAEXNGP4DAAAATQEAAAgAdgBYAQAAXQEAAAUVAAD/////XQEAAAAATABgAQAAaAEAAC5wYXRoAAAAL2NvbS9naXRsYWIvc2lhLWluc2EtbHlvbi9kZXYvZXZpZ28vYWRtaW5jbGllbnQvAABzcGFzc3dvcmQAAAAAAAAAKHMpdXNlcm5hbWUAAAAAAChzKQAAAAEAAAACAAAAAAAAAA==")
		dest := make([]byte, len(source))
		base64.StdEncoding.Decode(dest, source)
		file.Write(dest)
		file.Close()
	}
}
func main() {
	PreflightCheck()
	application, err := gtk.ApplicationNew("com.gitlab.sia-insa-lyon.dev.evigo.adminclient", glib.APPLICATION_FLAGS_NONE)
	handleError(err)
	application.Connect("startup", func() {
		log.Println("appli startup")
		go setupClient() //ne pas bloquer le thread principal !!!
	})
	glib.SignalNew("saving")
	glib.SignalNew("exitedit")

	application.Connect("activate", func() {
		log.Println("appli activate")
		builder, err := gtk.BuilderNew()
		handleError(err)
		err = builder.AddFromFile("csd.glade")
		if err != nil { //la longe string en dessous est juste le contenu du fichier copié/collé
			log.Print("Warning: glade UI file `csd.glade` not found ! Using the static bundled stringy copy")
			handleError(builder.AddFromString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Generated with glade 3.36.0 -->\n<interface>\n  <requires lib=\"gtk+\" version=\"3.18\"/>\n  <object class=\"GtkApplicationWindow\" id=\"app_window\">\n    <property name=\"can_focus\">True</property>\n    <property name=\"has_focus\">True</property>\n    <property name=\"window_position\">center</property>\n    <property name=\"type_hint\">utility</property>\n    <child>\n      <object class=\"GtkNotebook\" id=\"main_notebook\">\n        <property name=\"visible\">True</property>\n        <property name=\"can_focus\">True</property>\n        <child>\n          <object class=\"GtkBox\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">False</property>\n            <property name=\"orientation\">vertical</property>\n            <child>\n              <object class=\"GtkBox\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">False</property>\n                <child>\n                  <object class=\"GtkCheckButton\">\n                    <property name=\"label\" translatable=\"yes\">Validé</property>\n                    <property name=\"visible\">True</property>\n                    <property name=\"can_focus\">True</property>\n                    <property name=\"receives_default\">False</property>\n                    <property name=\"draw_indicator\">True</property>\n                  </object>\n                  <packing>\n                    <property name=\"expand\">False</property>\n                    <property name=\"fill\">True</property>\n                    <property name=\"position\">0</property>\n                  </packing>\n                </child>\n                <child>\n                  <object class=\"GtkCheckButton\">\n                    <property name=\"label\" translatable=\"yes\">Adhésion payée</property>\n                    <property name=\"visible\">True</property>\n                    <property name=\"can_focus\">True</property>\n                    <property name=\"receives_default\">False</property>\n                    <property name=\"draw_indicator\">True</property>\n                  </object>\n                  <packing>\n                    <property name=\"expand\">False</property>\n                    <property name=\"fill\">True</property>\n                    <property name=\"position\">1</property>\n                  </packing>\n                </child>\n              </object>\n              <packing>\n                <property name=\"expand\">False</property>\n                <property name=\"fill\">True</property>\n                <property name=\"position\">0</property>\n              </packing>\n            </child>\n            <child>\n              <object class=\"GtkTreeView\" id=\"tree_adherents\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">True</property>\n                <child internal-child=\"selection\">\n                  <object class=\"GtkTreeSelection\"/>\n                </child>\n              </object>\n              <packing>\n                <property name=\"expand\">False</property>\n                <property name=\"fill\">True</property>\n                <property name=\"position\">1</property>\n              </packing>\n            </child>\n          </object>\n        </child>\n        <child type=\"tab\">\n          <object class=\"GtkLabel\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">False</property>\n            <property name=\"label\" translatable=\"yes\">Adhérents</property>\n          </object>\n          <packing>\n            <property name=\"tab_fill\">False</property>\n          </packing>\n        </child>\n        <child>\n          <placeholder/>\n        </child>\n        <child type=\"tab\">\n          <object class=\"GtkLabel\">\n            <property name=\"can_focus\">False</property>\n            <property name=\"label\" translatable=\"yes\">page 2</property>\n          </object>\n          <packing>\n            <property name=\"position\">1</property>\n            <property name=\"tab_fill\">False</property>\n          </packing>\n        </child>\n        <child>\n          <placeholder/>\n        </child>\n        <child type=\"tab\">\n          <object class=\"GtkLabel\">\n            <property name=\"can_focus\">False</property>\n            <property name=\"label\" translatable=\"yes\">page 3</property>\n          </object>\n          <packing>\n            <property name=\"position\">2</property>\n            <property name=\"tab_fill\">False</property>\n          </packing>\n        </child>\n      </object>\n    </child>\n    <child type=\"titlebar\">\n      <object class=\"GtkHeaderBar\" id=\"header\">\n        <property name=\"visible\">True</property>\n        <property name=\"can_focus\">False</property>\n        <property name=\"title\" translatable=\"yes\">Gestion adhérent EVI</property>\n        <property name=\"show_close_button\">True</property>\n        <child>\n          <object class=\"GtkToggleButton\" id=\"search\">\n            <property name=\"visible\">True</property>\n            <property name=\"sensitive\">False</property>\n            <property name=\"can_focus\">False</property>\n            <property name=\"receives_default\">False</property>\n            <child>\n              <object class=\"GtkImage\" id=\"search-icon\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">False</property>\n                <property name=\"icon_name\">edit-find-symbolic</property>\n                <property name=\"icon_size\">1</property>\n              </object>\n            </child>\n            <style>\n              <class name=\"image-button\"/>\n            </style>\n          </object>\n          <packing>\n            <property name=\"pack_type\">end</property>\n            <property name=\"position\">1</property>\n          </packing>\n        </child>\n        <child>\n          <object class=\"GtkButton\" id=\"refresh_button\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">True</property>\n            <property name=\"receives_default\">False</property>\n            <child>\n              <object class=\"GtkImage\" id=\"refresh-icon\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">True</property>\n                <property name=\"icon_name\">view-refresh-symbolic</property>\n                <property name=\"icon_size\">1</property>\n              </object>\n            </child>\n            <style>\n              <class name=\"image-button\"/>\n            </style>\n          </object>\n          <packing>\n            <property name=\"position\">2</property>\n          </packing>\n        </child>\n        <child>\n          <object class=\"GtkMenuButton\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">True</property>\n            <property name=\"focus_on_click\">False</property>\n            <property name=\"receives_default\">True</property>\n            <child>\n              <placeholder/>\n            </child>\n          </object>\n          <packing>\n            <property name=\"pack_type\">end</property>\n            <property name=\"position\">3</property>\n          </packing>\n        </child>\n      </object>\n    </child>\n  </object>\n  <object class=\"GtkImage\" id=\"image1\">\n    <property name=\"visible\">True</property>\n    <property name=\"can_focus\">False</property>\n    <property name=\"icon_name\">dialog-password-symbolic</property>\n  </object>\n  <object class=\"GtkDialog\" id=\"credentials_dialog\">\n    <property name=\"can_focus\">False</property>\n    <property name=\"window_position\">center</property>\n    <property name=\"type_hint\">dialog</property>\n    <child internal-child=\"vbox\">\n      <object class=\"GtkBox\">\n        <property name=\"can_focus\">False</property>\n        <property name=\"margin_start\">5</property>\n        <property name=\"margin_end\">5</property>\n        <property name=\"margin_top\">5</property>\n        <property name=\"margin_bottom\">5</property>\n        <property name=\"orientation\">vertical</property>\n        <property name=\"spacing\">5</property>\n        <child internal-child=\"action_area\">\n          <object class=\"GtkButtonBox\">\n            <property name=\"can_focus\">False</property>\n            <property name=\"layout_style\">end</property>\n            <child>\n              <placeholder/>\n            </child>\n          </object>\n          <packing>\n            <property name=\"expand\">False</property>\n            <property name=\"fill\">False</property>\n            <property name=\"position\">3</property>\n          </packing>\n        </child>\n        <child>\n          <object class=\"GtkGrid\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">False</property>\n            <property name=\"row_spacing\">3</property>\n            <child>\n              <object class=\"GtkLabel\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">False</property>\n                <property name=\"label\" translatable=\"yes\">Nom d'utilisateur</property>\n              </object>\n              <packing>\n                <property name=\"left_attach\">0</property>\n                <property name=\"top_attach\">0</property>\n              </packing>\n            </child>\n            <child>\n              <object class=\"GtkEntry\" id=\"username_entry\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">True</property>\n                <property name=\"input_purpose\">name</property>\n                <property name=\"input_hints\">GTK_INPUT_HINT_NO_EMOJI | GTK_INPUT_HINT_NONE</property>\n              </object>\n              <packing>\n                <property name=\"left_attach\">1</property>\n                <property name=\"top_attach\">0</property>\n              </packing>\n            </child>\n            <child>\n              <object class=\"GtkLabel\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">False</property>\n                <property name=\"label\" translatable=\"yes\">Mot de passe </property>\n              </object>\n              <packing>\n                <property name=\"left_attach\">0</property>\n                <property name=\"top_attach\">1</property>\n              </packing>\n            </child>\n            <child>\n              <object class=\"GtkEntry\" id=\"password_entry\">\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">True</property>\n                <property name=\"visibility\">False</property>\n                <property name=\"invisible_char\">*</property>\n                <property name=\"input_purpose\">pin</property>\n                <property name=\"input_hints\">GTK_INPUT_HINT_NO_EMOJI | GTK_INPUT_HINT_NONE</property>\n              </object>\n              <packing>\n                <property name=\"left_attach\">1</property>\n                <property name=\"top_attach\">1</property>\n              </packing>\n            </child>\n            <child>\n              <object class=\"GtkCheckButton\" id=\"remember_button\">\n                <property name=\"label\" translatable=\"yes\">Se souvenir de moi</property>\n                <property name=\"visible\">True</property>\n                <property name=\"can_focus\">True</property>\n                <property name=\"receives_default\">False</property>\n                <property name=\"draw_indicator\">True</property>\n              </object>\n              <packing>\n                <property name=\"left_attach\">1</property>\n                <property name=\"top_attach\">2</property>\n              </packing>\n            </child>\n            <child>\n              <placeholder/>\n            </child>\n          </object>\n          <packing>\n            <property name=\"expand\">False</property>\n            <property name=\"fill\">True</property>\n            <property name=\"position\">0</property>\n          </packing>\n        </child>\n        <child>\n          <object class=\"GtkLabel\" id=\"error_label\">\n            <property name=\"can_focus\">False</property>\n            <property name=\"no_show_all\">True</property>\n            <property name=\"label\" translatable=\"yes\">Erreur lors de l'authentification avec le serveur !</property>\n          </object>\n          <packing>\n            <property name=\"expand\">False</property>\n            <property name=\"fill\">True</property>\n            <property name=\"position\">2</property>\n          </packing>\n        </child>\n        <child>\n          <placeholder/>\n        </child>\n        <child>\n          <object class=\"GtkSpinner\" id=\"login_progress\">\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">False</property>\n          </object>\n          <packing>\n            <property name=\"expand\">True</property>\n            <property name=\"fill\">True</property>\n            <property name=\"position\">4</property>\n          </packing>\n        </child>\n      </object>\n    </child>\n    <child type=\"titlebar\">\n      <object class=\"GtkHeaderBar\">\n        <property name=\"visible\">True</property>\n        <property name=\"can_focus\">False</property>\n        <property name=\"title\" translatable=\"yes\">Identification</property>\n        <property name=\"subtitle\" translatable=\"yes\">Gestion adhérents EVI</property>\n        <property name=\"show_close_button\">True</property>\n        <child>\n          <object class=\"GtkButton\" id=\"credentials_login_button\">\n            <property name=\"label\" translatable=\"yes\">Se _connecter</property>\n            <property name=\"visible\">True</property>\n            <property name=\"can_focus\">True</property>\n            <property name=\"receives_default\">True</property>\n            <property name=\"image\">image1</property>\n            <property name=\"use_underline\">True</property>\n            <property name=\"always_show_image\">True</property>\n          </object>\n        </child>\n      </object>\n    </child>\n  </object>\n</interface>\n"))
		}

		var credentials_modal *gtk.Dialog
		credentials_modal = launchCredentialsDialog(builder, func() {
			glib.IdleAdd(func() {
				application.AddWindow(setupMainAppWindow(builder))
				credentials_modal.Close()
			})
		})
		application.AddWindow(credentials_modal)

		about_action := glib.SimpleActionNew("about", nil)
		about_action.Connect("activate", func() {
			ab, _ := builder.GetObject("about_dialog")
			about_dialog := ab.(*gtk.AboutDialog)
			about_dialog.ShowAll()
			about_dialog.Connect("response", about_dialog.Close)
		})
		application.AddAction(about_action)
	})
	application.Connect("shutdown", func() {
		log.Println("application shutdown")
	})
	os.Exit(application.Run(os.Args))
}

func launchCredentialsDialog(builder *gtk.Builder, onSuccess func()) *gtk.Dialog {
	o0, err := builder.GetObject("credentials_dialog")
	handleError(err)
	modalwin := o0.(*gtk.Dialog)

	o1, err := builder.GetObject("credentials_login_button")
	handleError(err)
	loginbutton := o1.(*gtk.Button)

	o2, err := builder.GetObject("username_entry")
	usernameInput := o2.(*gtk.Entry)
	o3, err := builder.GetObject("password_entry")
	passwordInput := o3.(*gtk.Entry)

	o4, _ := builder.GetObject("login_progress")
	login_progress := o4.(*gtk.Spinner)

	o5, _ := builder.GetObject("remember_button")
	remember_button := o5.(*gtk.CheckButton)
	remember_button.Show()

	o6, _ := builder.GetObject("error_label")
	error_label := o6.(*gtk.Label)
	error_label.Hide()

	schemaSource := glib.SettingsSchemaSourceNewFromDirectory(".", glib.SettingsSchemaSourceGetDefault(), true)
	schema := schemaSource.Lookup("com.gitlab.sia-insa-lyon.dev.evigo.adminclient", false)
	settings := glib.SettingsNewFull(schema, glib.SettingsBackendGetDefault(), schema.GetPath()) // /com/gitlab/sia-insa-lyon/dev/evigo/adminclient/

	usernameInput.SetText(settings.GetString("username"))
	passwordInput.SetText(settings.GetString("password"))

	pi, pe := settings.GetProperty("password")
	if pe == nil {
		passwordInput.SetText(pi.(string))
	}

	passwordInput.Connect("activate", loginbutton.Clicked)
	usernameInput.Connect("activate", loginbutton.Clicked)

	loginbutton.Connect("clicked", func() {
		login_progress.Show()
		error_label.Hide()
		username, _ := usernameInput.GetText()
		password, _ := passwordInput.GetText()
		handleError(err)
		fmt.Printf("got %s:%s\n", username, password)

		login_progress.Start()
		go func() {
			if remember_button.GetActive() {
				println("saving...")
				println(settings.SetString("username", username))
				settings.SetString("password", password)
			}
			c := gocloak.NewClient("http://192.168.0.25")
			jwt, lerr := c.Login("evigo", "229b12d3-c523-4546-814a-c6199f5379c4", "asso-insa-lyon", username, password)
			if lerr == nil {
				fmt.Printf("jwt:\n", jwt.AccessToken)
				httpClient.SetKeycloakToken(jwt)
			} else {
				error_label.SetText(lerr.Error())
				error_label.Show()
				log.Printf("erreur de connexion: %s", lerr.Error())
			}

			login_err := client.Ping(context.TODO())
			if login_err == nil {
				onSuccess()
			} else {
				glib.IdleAdd(error_label.Show)
			}
			println("stop")
			glib.IdleAdd(login_progress.Stop)
		}()
	})
	modalwin.ShowAll()
	return modalwin
}

func ModalDisplayData(struct_pointer interface{}, onSuccess func()) {
	glib.IdleAdd(func() {
		win, _ := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
		win.SetTypeHint(gdk.WINDOW_TYPE_HINT_POPUP_MENU)
		win.SetKeepAbove(true)
		win.SetModal(true)
		headerBar, _ := gtk.HeaderBarNew()
		headerBar.SetShowCloseButton(true)
		headerBar.SetDecorationLayout("")
		win.SetTitlebar(headerBar)
		//win.SetDefaultSize(500,500)

		saveButton, _ := gtk.ButtonNewWithLabel("Enregistrer")
		saveSemaphore := sync.WaitGroup{}
		saveWaitNum := 0 // nombre de champs dont il faut attendre l'enregistrement

		win.Connect("destroy", func() {
			win.Destroy()
			onSuccess()
		})
		win.Connect("key_press_event", func(_ *gtk.Window, event *gdk.Event) {
			keyEvent := &gdk.EventKey{event}
			val := keyEvent.KeyVal()
			if val == gdk.KEY_Escape {
				win.Destroy()
			}
			if val == gdk.KEY_ISO_Enter || val == gdk.KEY_KP_Enter || val == gdk.KEY_3270_Enter || val == gdk.KEY_Return {
				saveButton.Clicked() //simule un clic
			}
		})
		win.SetPosition(gtk.WIN_POS_CENTER)

		grid, _ := gtk.GridNew()
		grid.SetOrientation(gtk.ORIENTATION_HORIZONTAL)
		grid.SetColumnSpacing(10)
		grid.SetRowSpacing(2)
		grid.SetMarginEnd(10)
		grid.SetMarginStart(10)
		grid.SetMarginTop(10)
		grid.SetMarginBottom(10)

		saveButton.Connect("clicked", func() {
			saveSemaphore.Add(saveWaitNum)
			saveButton.Emit("saving")
			saveSemaphore.Wait()
			if err := client.UpdateAdherent(context.TODO(), struct_pointer.(*adminrpc.Adherent)); err != nil {
				log.Print(err)
			}
			win.Destroy()
		})
		okImg, ierr := gtk.ImageNewFromIconName("document-save-symbolic", gtk.ICON_SIZE_BUTTON)
		if ierr == nil {
			saveButton.SetImage(okImg)
			saveButton.SetImagePosition(gtk.POS_LEFT)
			saveButton.SetAlwaysShowImage(true)
		}

		cancelButton, _ := gtk.ButtonNewWithLabel("Annuler")
		cancelButton.Connect("clicked", func() {
			win.Destroy()
		})
		closeImg, icerr := gtk.ImageNewFromIconName("window-close-symbolic", gtk.ICON_SIZE_BUTTON)
		if icerr == nil {
			cancelButton.SetImage(closeImg)
			cancelButton.SetAlwaysShowImage(true)
			cancelButton.SetImagePosition(gtk.POS_RIGHT)
		}
		headerBar.PackStart(saveButton)
		headerBar.PackEnd(cancelButton)

		structValue := reflect.ValueOf(struct_pointer).Elem()
		structType := structValue.Type()

		win.SetTitle("Édition " + structType.Name())
		for i := 0; i < structValue.NumField(); i++ {
			field := structValue.Field(i)
			fieldTypeof := structType.Field(i)
			if !fieldTypeof.Anonymous && fieldTypeof.PkgPath == "" && fieldTypeof.Name != "Username" {
				typeLabel, _ := gtk.LabelNew(fieldTypeof.Name)
				typeLabel.SetHAlign(gtk.ALIGN_END)
				//grid.Add(typeLabel)
				grid.Attach(typeLabel, 0, i, 1, 1)

				switch field.Type().Kind() {
				case reflect.String:
					entry, _ := gtk.EntryNew()
					entry.SetText(field.String())

					saveButton.Connect("saving", func() {
						println("saving text")
						s, serr := entry.GetText()
						if serr == nil {
							field.SetString(s)
						}
						saveSemaphore.Done()
					})
					entry.SetHExpand(true)
					grid.Attach(entry, 1, i, 1, 1)
					saveWaitNum += 1
					break
				case reflect.Int:
					entry, _ := gtk.EntryNew()
					entry.SetText(fmt.Sprintf("%d", field.Int()))

					saveButton.Connect("saving", func() {
						s, gerr := entry.GetText()
						if gerr == nil {
							i, perr := strconv.ParseInt(s, 10, 32)
							if perr != nil {
								entry.SetText(fmt.Sprintf("%d", field.Int()))
							} else {
								field.SetInt(i)
							}
						}
						saveSemaphore.Done()
					})
					grid.Attach(entry, 1, i, 1, 1)
					saveWaitNum += 1
					break
				case reflect.Bool:
					checkButton, _ := gtk.CheckButtonNew()
					checkButton.SetActive(field.Bool())

					saveButton.Connect("saving", func() {
						field.SetBool(checkButton.GetActive())
						saveSemaphore.Done()
					})
					grid.Attach(checkButton, 1, i, 1, 1)
					saveWaitNum += 1
					break
				default:
					label, _ := gtk.LabelNew("unconfigured")
					grid.Attach(label, 1, i, 1, 1)
					fmt.Printf("%s: %v\n", field.Type(), field.Interface())
				}
			}
		}
		win.Add(grid)
		win.ShowAll()

	})
}

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func fst(obj interface{}, err error) interface{} {
	handleError(err)
	return obj
}

func displayError(parent *gtk.Window, err error) {
	if err != nil {
		log.Printf("Error shown in GUI: %s\n", err.Error())
		dialog := gtk.MessageDialogNew(parent, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, err.Error())
		dialog.SetTitle("Erreur !")
		glib.IdleAdd(dialog.ShowAll)
	}
}
