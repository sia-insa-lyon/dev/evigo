package web

import (
	"evigo/db"
	"fmt"
	"github.com/go-macaron/csrf"
	"github.com/go-macaron/session"
	"gopkg.in/macaron.v1"
	"log"
)

func showError(ctx *macaron.Context, err error) {
	if err != nil {
		log.Print(err)
		ctx.Data["Err"] = err
		ctx.HTML(500, "error")
	}
}

func Index(ctx *macaron.Context) {
	ctx.Data["Variable"] = "valeur"
	ctx.HTML(200, "index")
}

func MyProfile(ctx *macaron.Context, sess session.Store) {
	adherent, err := db.FindAdherentByUsername(fmt.Sprintf("%s", sess.Get("username")))
	showError(ctx, err)
	ctx.Data["Adherent"] = *adherent
	ctx.HTML(200, "profile")
}

func TableauAdherents(ctx *macaron.Context, x csrf.CSRF) {
	valides := db.ListValidAdherents()
	impayes := db.ListUnpaidAdherents()
	invalides := db.ListInvalidAdherents()

	ctx.Data["Valides"] = valides
	ctx.Data["Impayes"] = impayes
	ctx.Data["Invalides"] = invalides

	ctx.Data["csrf_token"] = x.GetToken()

	ctx.HTML(200, "gestion/liste")
}

func ModifPaiement(ctx *macaron.Context) {
	form := ctx.Req.Form
	fmt.Printf("data: %s\n %v\n", form, form)
	modifs := []string{}
	for name := range ctx.Req.Form {
		if name == "_csrf" {
			continue
		}
		modifs = append(modifs, name)
	}
	if len(modifs) > 0 {
		db.UpdateMultiplePayments(modifs)
	}
	ctx.Redirect(ctx.URLFor("adherents_liste"))
}

func ValidateAdherents(ctx *macaron.Context) {
	form := ctx.Req.Form
	fmt.Printf("data: %s\n %v\n", form, form)
	modifs := []string{} // les usernames des personnes à valider
	for name := range ctx.Req.Form {
		if name == "_csrf" {
			continue
		}
		modifs = append(modifs, name)
	}
	fmt.Printf("%s\n", modifs)
	db.UpdateMultipleValidations(modifs)
	ctx.Redirect(ctx.URLFor("adherents_liste"))
}

func RechercheUsersKC(ctx *macaron.Context, x csrf.CSRF) {
	ctx.Data["csrf_token"] = x.GetToken()
	ctx.HTML(200, "gestion/recherche")
}
func RechercheUsersKcRresult(ctx *macaron.Context, x csrf.CSRF) {
	ctx.Data["csrf_token"] = x.GetToken()

	email := ctx.Req.Form["emailValue"]
	ctx.Data["Resultats"] = true
	ctx.Data["Users"] = SearchKCUsersByEmail(email[0])
	ctx.HTML(200, "gestion/recherche")
}

func AjoutUser(ctx *macaron.Context) {
	println(ctx.Req.Form["username"][0])
	println(ctx.Req.Form["id"][0])
	user := GetKCUserById(ctx.Req.Form["id"][0])
	ad, er := db.FindAdherentByUsername(*user.Username)
	if ad != nil || er == nil {
		// l'utilisateur existe déjà
		ctx.Redirect(ctx.URLFor("adherent_edit", ":username", *user.Username))
		return
	}
	adherent := db.InitAdherent()
	adherent.UserToAdherent(user)

	db.CreateAdherent(adherent)

	ctx.Redirect(ctx.URLFor("adherent_edit", ":username", adherent.Username))
}

func EditAdherentForm(ctx *macaron.Context, x csrf.CSRF) {
	adherent, err := db.FindAdherentByUsername(ctx.Params(":username"))
	showError(ctx, err)
	ctx.Data["adherent"] = adherent
	ctx.Data["csrf_token"] = x.GetToken()
	ctx.HTML(200, "gestion/adherent-form")
}

func EditAdherent(ctx *macaron.Context, form db.AdherentForm) {
	username := ctx.Params(":username")
	fmt.Printf("%v\n", form)
	adherent, err := db.FindAdherentByUsername(username)
	showError(ctx, err)
	adherent.ApplyFormData(&form)
	db.UpdateAdherent(adherent)
	ctx.Redirect(ctx.URLFor("adherent_edit", ":username", username))
}
func ExportMailingSympa(ctx *macaron.Context) {
	ctx.Data["adherents"] = db.ListValidAdherents()
	ctx.HTML(200, "gestion/export-sympa")
}
