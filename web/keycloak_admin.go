package web

import (
	"fmt"
	"github.com/Nerzal/gocloak/v5"
	"log"
	"time"
)

var client gocloak.GoCloak
var token *gocloak.JWT

var EviGroupID string
var EviBureauID string
var ClientAdminID string // l'id du client

func SetupAdminClient() {
	client = gocloak.NewClient(KeycloakUrl)
	//users, err := client.GetUsers(token.AccessToken, "asso-insa-lyon", gocloak.GetUsersParams{})
	//users, err := client.GetUsers(token.AccessToken, "asso-insa-lyon", gocloak.GetUsersParams{Email: ""})
	//démontre que c'est interdit par la permission view-users
	/*user,err:=client.GetUserByID(token.AccessToken, "asso-insa-lyon", "6e22bf51-b91c-4b9a-a677-fb52689aff00")
	handleError(err)
	fmt.Printf("%s %s - %s %s\n", user.Email, user.Username, user.FirstName, user.LastName)
	handleError(err)*/
	AuthenticateAdmin()
	RetrieveMetadata()
	go Refresh()
	//go RefreshRefreshToken()

	//PromoteBureau("dv0de7sdlphjqri8nnvrlayyy4q")

}
func AuthenticateAdmin() {
	log.Print("Authenticating Keycloak admin ....")
	var err error
	token, err = client.LoginClient(ClientId, ClientSecret, Realm)
	if err != nil {
		log.Fatal(err)
	}
	log.Print(" Authenticated Keycloak admin [ok]")
}

/* à utiliser si les refresh tokens remarchent
func RefreshRefreshToken() {
	time.Sleep(time.Duration(token.RefreshExpiresIn-60) * time.Second)
	AuthenticateAdmin()
	RefreshRefreshToken()
}*/

func Refresh() {
	time.Sleep((time.Duration(token.ExpiresIn) - 30) * time.Second)
	/*
		à utiliser si les refresh tokens remarchent
		var rerr error
		log.Print("refreshing Keycloak admin token ....")
		token, rerr = client.RefreshToken(token.RefreshToken, ClientId, ClientSecret, Realm)
		if rerr != nil {
			log.Fatal(rerr)
		}
		log.Print(" refreshed Keycloak admin token [ok]")*/
	AuthenticateAdmin() //à enlever si les refresh tokens marchent
	Refresh()
	log.Print("refresh lööp ended")
}

var ASSOS = "ASSOS"

func RetrieveMetadata() {
	groups, _ := client.GetGroups(token.AccessToken, Realm, gocloak.GetGroupsParams{Search: &ASSOS})
	assosGroup := groups[0]
	for _, g := range assosGroup.SubGroups {
		fmt.Printf("%s\n", *g.Name)
		if *g.Path == "/ASSOS/EVI" {
			EviGroupID = *g.ID
			for _, sg := range g.SubGroups {
				if *sg.Name == "Bureau" {
					EviBureauID = *sg.ID
					break
				}
			}
			break
		}
	}
	fmt.Printf("EVI id : %s, EVI/Bureau %s\n", EviGroupID, EviBureauID)

	//clientData, _ := client.GetClient(token.AccessToken, Realm, ClientId)
	//handleError(err)

	clients, err := client.GetClients(token.AccessToken, Realm, gocloak.GetClientsParams{ClientID: &ClientId})
	handleError(err)
	for _, c := range clients {
		fmt.Printf("%s --- %s --- %s\n", *c.ID, *c.Name, *c.ClientID)
		if *c.ClientID == "evigo" {
			ClientAdminID = *c.ID
		}
	}
}

func SearchKCUsersByEmail(email string) []*gocloak.User {
	users, err := client.GetUsers(token.AccessToken, Realm, gocloak.GetUsersParams{Email: &email})
	handleError(err)
	return users
}

func GetKCUserById(id string) *gocloak.User {
	// &{6e22bf51-b91c-4b9a-a677-fb52689aff00 %!s(int64=1588971800117) dv0de7sdlphjqri8nnvrlayyy4q %!s(bool=true) %!s(bool=false) %!s(bool=true) jean ribes jean.christophe.ribes@sfr.fr  map[adhesionUserId:[24] birthday:[2020-05-19] category:[student] department:[FIMI] gender:[M] locale:[fr] memberId:[32] school:[INSA] study_year:[1A] terms_and_conditions:[1588971845]] [] [] map[impersonate:%!s(bool=false) manage:%!s(bool=false) manageGroupMembership:%!s(bool=false) mapRoles:%!s(bool=false) view:%!s(bool=true)]}
	user, err := client.GetUserByID(token.AccessToken, Realm, id)
	handleError(err)
	return user
}

func PromoteBureau(username string) {
	users, _ := client.GetUsers(token.AccessToken, Realm, gocloak.GetUsersParams{Username: &username})
	user := users[0]

	mr, rerr := client.GetRoleMappingByGroupID(token.AccessToken, Realm, EviBureauID)
	handleError(rerr)
	var RoleBureau gocloak.Role
	for _, rolemappings := range mr.ClientMappings {
		for _, role := range rolemappings.Mappings {
			if *role.Name == "bureau" {
				RoleBureau = *role
			}
		}
	}
	fmt.Printf("%s : %s\n", *RoleBureau.Name, *RoleBureau.ID)

	handleError(client.AddClientRoleToUser(token.AccessToken, Realm, ClientAdminID, *user.ID, []gocloak.Role{RoleBureau}))

	//client.AddUserToGroup()
}
