package web

import (
	"evigo/db"
	"github.com/go-macaron/binding"
	"github.com/go-macaron/csrf"
	"gopkg.in/macaron.v1"
)

func SetupRoutes(m *macaron.Macaron) {
	m.Get("/", Index).Name("index")

	//authentification Keycloak
	m.Get("/start/", OidcStart).Name("oidc_start")
	m.Get("/return/", OidcFinish).Name("oidc_finish")
	m.Get("/disconnect", AuthenticationMiddleware, Disconnect).Name("disconnect")
	m.Get("/profile", AuthenticationMiddleware, MyProfile).Name("my_profile")

	m.Get("/adherents/", BureauMiddleware, TableauAdherents).Name("adherents_liste")
	m.Get("/adherents/export/", BureauMiddleware, ExportMailingSympa).Name("adherents_exportsympa")
	m.Post("/adherents/modifpay/", csrf.Validate, BureauMiddleware, ModifPaiement).Name("adherents_pay")
	m.Post("/adherents/validations/", csrf.Validate, BureauMiddleware, ValidateAdherents).Name("adherents_validate")

	m.Get("/adherent/:username/edit", BureauMiddleware, EditAdherentForm).Name("adherent_edit")
	m.Post("/adherent/:username/edit", BureauMiddleware, csrf.Csrfer(), binding.Bind(db.AdherentForm{}), EditAdherent) //même route mais en POST

	m.Get("/keycloak/recherche/", BureauMiddleware, RechercheUsersKC).Name("keycloak_search")
	m.Post("/keycloak/recherche/", csrf.Validate, BureauMiddleware, RechercheUsersKcRresult).Name("keycloak_searchP")
	m.Post("/keycloak/ajout/", csrf.Validate, BureauMiddleware, AjoutUser).Name("keycloak_addfrom")
}
