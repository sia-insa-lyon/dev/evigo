# Gestion adhérents EVI changelog

The latest version of this file can be found at the master branch of the Gestion adhérents EVI repository.
## 0.2.0
### Removed (1 change)
- MongoDB official driver
### Changed (1 change)
- templates and UI
### Added (1 change)
- Mogo, a MongoDB ODM (NoSQL ORM)
## 0.1.0

### Removed (0 change)

### Fixed (0 changes, 0 of them are from the community)

### Changed (0 change)

### Added (X changes)

- Adding CI/CD and environment configuration (Staging and Deployment)
- Adding issue and MR templates
- Adding Changelog, Contributing

### Other (0 change)

